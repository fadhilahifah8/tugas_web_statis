@extends('adminlte.master')

    @section('judul1')
    <div class="ml-3 mt-3">
        <h1>Tambah Cast</h1>
    </div>
      
    <div class="ml-3 mt-3"> 
        <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Isilah Biodata di Bawah Ini!</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" action="/cast" method="POST">
                      @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama name="nama placeholder="Nama Lengkap">
                        @error('nama')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                      </div>

                      <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="text" class="form-control" id="umur" nama="umur" placeholder="Umur">
                        @error('umur')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                      </div>

                      <div class="form-group">
                        <label for="bio">Bio</label>
                        <input type="text" class="form-control" id="bio" nama="bio" placeholder="bio">
                        @error('bio')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                      </div>

                    </div>
                    <!-- /.card-body -->
    
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
    </div>
    @endsection

