<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('Register');
    }

    public function welcome2_post(Request $request) {
        $nama = $request->nama;
        return view('Welcome2', compact('nama'));

    }
}
